#include "mainwindow.h"
#include <QOpenGLFunctions>
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(0,0,0,0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

